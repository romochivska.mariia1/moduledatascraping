import json
from requests import get
from bs4 import BeautifulSoup

url = "https://dspace.uzhnu.edu.ua/jspui/handle/123456789/43/browse?type=author&order=ASC&rpp=20&offset=0"

data = []

page = get(url)
soup = BeautifulSoup(page.content, "html.parser")
scientist_list = soup.find(class_="list-group")

for li in scientist_list.find_all("li"):
    scientist_data = {}
    a = li.find("a")
    scientist_name = a.string
    scientist_link = url + a.get("href")
    scientist_data["scientist_name"] = scientist_name
    scientist_data["scientist_link"] = scientist_link

    scientist_page = get(scientist_link)
    scientist_soup = BeautifulSoup(scientist_page.content, "html.parser")

    book_table = scientist_soup.find("table", class_="table")
    if book_table:
        for tr in book_table.find_all("tr"):
            book_name_element = tr.a
            if book_name_element:
                book_name = book_name_element.string.strip()
                scientist_data["book_name"] = book_name
                data.append(scientist_data.copy())

FILE_NAME = "archive.json"
with open(FILE_NAME, "w", encoding="utf-8") as json_file:
    json.dump(data, json_file, indent=4, ensure_ascii=False)
